﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Ferreteria_forms_vendedor_InicioVentas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnVenta_Click(object sender, EventArgs e)
    {
        Random rnd = new Random();
        int codVent = rnd.Next(10, 999999999);
        Session.Add("id_Venta", codVent.ToString());
        Response.Redirect("BuscarProductos.aspx");
    }
}