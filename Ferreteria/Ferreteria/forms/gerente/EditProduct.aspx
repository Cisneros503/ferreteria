﻿<%@ Page Title="" Language="C#" MasterPageFile="../../maestras/Gerente.master" AutoEventWireup="true" CodeFile="EditProduct.aspx.cs" Inherits="Ferreteria_forms_gerente_EditProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br/>
    <br/>
    <br/>
    <br/>
    <center>
        <table>
            <tr>
                <td colspan="2"><h3>Editar Productos</h3></td>
            </tr>
            <tr>
                <td><asp:DropDownList id="dropcat" runat="server"></asp:DropDownList></td>
                <td><asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click"></asp:Button></td>
            </tr>
            <tr>
                <td colspan="2"><asp:GridView ID="gridprod" runat="server" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical" OnSelectedIndexChanging="gridprod_SelectedIndexChanging">
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                    <Columns>
                        <asp:CommandField HeaderText="Acción" ShowHeader="True" ShowSelectButton="True" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                    </asp:GridView></td>
            </tr>
            <tr>
                <td>Imagen:</td>
                <td><asp:FileUpload ID="nsubir" runat="server"></asp:FileUpload></td>
            </tr>
            <tr>
                <td colspan="2"><asp:Image ID="ncarg" runat="server" Height="200px" Width="200px"></asp:Image></td>
            </tr>
            <tr>
                <td colspan="2"><asp:Button ID="ncarga" runat="server" Text="Cargar" OnClick="ncarga_Click"></asp:Button></td>
            </tr>
             <tr>
                <td>Categoria:</td>
                <td><asp:DropDownList ID="dropncat" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td>Nombre:</td>
                <td><asp:Textbox ID="txtnnombre" runat="server" placeholder="Nuevo nombre"></asp:Textbox></td>
            </tr>
            <tr>
                <td>Precio:</td>
                <td><asp:Textbox ID="txtprecio" runat="server" placeholder="$0.00"></asp:Textbox></td>
            </tr>
            <tr>
                <td>Descripcion:</td>
                <td><asp:Textbox ID="txtndescrip" runat="server" Placeholder="Descripcion.." Height="43px" TextMode="MultiLine"></asp:Textbox></td>
            </tr>
            <tr>
                <td>Tipo:</td>
                <td><asp:Textbox ID="txtntipo" runat="server" Placeholder="Tipo"></asp:Textbox></td>
            </tr>
            <tr>
                <td>Estado</td>
                <td><asp:DropDownList ID="dropest" runat="server">
                    <asp:ListItem>Activo</asp:ListItem>
                    <asp:ListItem>Inactivo</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td>Proveedor:</td>
                <td><asp:DropDownList ID="ndropprov" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td><asp:Button ID="btnActualizar" runat="server" Text="Actualizar" OnClick="btnActualizar_Click"></asp:Button></td>
                <td><asp:Button ID="btnCancelar" runat="server" Text="Cancelar"></asp:Button></td>
            </tr>
            <tr>
                <td colspan="2"><asp:Label ID="lblres" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2"><asp:label ID="lblid" runat="server" Visible="false"></asp:label></td>
            </tr>
            </table>
        <br/>
         <br/>
         <br/>
         <br/>
    </center>
</asp:Content>

