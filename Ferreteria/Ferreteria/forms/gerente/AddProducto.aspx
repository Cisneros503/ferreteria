﻿<%@ Page Title="" Language="C#" MasterPageFile="../../maestras/Gerente.master" AutoEventWireup="true" CodeFile="AddProducto.aspx.cs" Inherits="Ferreteria_forms_gerente_AddProducto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br/>
     <br/>
     <br/>
     <br/>
    <center>
        <table>
            <tr>
                <td colspan="2"><h3>Agregar Producto</h3></td>
            </tr>
            <tr>
                <td colspan="2"><asp:FileUpload ID="subir" runat="server"></asp:FileUpload></td>
            </tr>
            <tr>
                <td colspan="2"><asp:Image ID="imgc" runat="server" Height="200px" Width="200px"></asp:Image></td>
            </tr>
            <tr>
               <td><asp:Button ID="Carga" runat="server" Text="Cargar Imagen" OnClick="Carga_Click"></asp:Button> </td>
            </tr>
            <tr>
                <td>Categoria:</td>
                <td><asp:DropDownList ID="dropcat" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td>Nombre:</td>
                <td><asp:Textbox ID="txtnom" runat="server" placeholder="Nombre"></asp:Textbox></td>
            </tr>
            <tr>
                <td>Precio:</td>
                <td><asp:Textbox id="txtPrecio" runat="server" placeholder="$0.00"></asp:Textbox></td>
            </tr>
            <tr>
                <td>Descripcion:</td>
                <td><asp:Textbox ID="txtdes" runat="server" Placeholder="Descripcion..." Height="37px" TextMode="MultiLine"></asp:Textbox></td>
            </tr>
            <tr>
                <td>Tipo:</td>
                <td><asp:Textbox ID="txttipo" runat="server" placeholder="Tipo"></asp:Textbox></td>
            </tr>
            <tr>
                <td>Estado:</td>
                <td><asp:DropDownList ID="Dropesta" runat="server">
                    <asp:ListItem>Activo</asp:ListItem>
                    <asp:ListItem>Inactivo</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td>Proveedor:</td>
                <td><asp:DropDownList ID="dropprov" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td><asp:Button id="btnAgregar" runat="server" Text="Agregar" OnClick="btnAgregar_Click"></asp:Button></td>
                <td><asp:Button ID="btnCancelar" runat="server" Text="Cancelar"></asp:Button></td>
            </tr>
            <tr>
                <td colspan="2"><asp:Label ID="lblres" runat="server"></asp:Label></td>
            </tr>
        </table>
    </center>
    <br/>
     <br/>
     <br/>
     <br/>

</asp:Content>

