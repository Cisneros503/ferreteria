﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Ferreteria_forms_gerente_ModificarUsuario : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnCosultar_Click(object sender, EventArgs e)
    {
        // Definimos una cadena y le asignamos la cadena de conexión definida en el archivo Web.config
        string cadena = System.Configuration.ConfigurationManager.ConnectionStrings["VentasConnectionString"].ConnectionString;
        /* Creamos un objeto de la clase SQLConnection indicando como parámetro la cadena de conexión
        creada anteriormente */
        SqlConnection conexion = new SqlConnection(cadena);
        // Abrimos la conexión
        conexion.Open();
        // Creamos un objeto de la clase SqlCommand con el dato cargado en el control TextBox
        SqlCommand comando = new SqlCommand("select Username, Email from Usuario " + " where Username='" +
        txtUsuario.Text + "'", conexion);
        /* Creamos un objeto de la clase SqlDataReader e inicializándolo mediante la llamada del
        método ExecuteReader de la clase SQLCommand */
        SqlDataReader registro = comando.ExecuteReader();
        /* Recorremos el SqlDataReader (como este caso puede retornar cero o un registro) lo hacemos
        mediante un "if" */
        /* Si el método Read retorna true luego podemos acceder a la fila recuperada con el select y
        la mostramos en los TextBox */
        if (registro.Read())
        {
            txtUsuario1.Text = registro["Username"].ToString();            
            txtEmail.Text = registro["Email"].ToString();
            
        }
        // Si retorna "False" mostramos un mensaje informativo
        else
            lblMensaje.Text = "No existe un Usuario con el nombre indicado. ";
        // Cerramos la conexión
        conexion.Close();
    }
    protected void BtnModificar_Click(object sender, EventArgs e)
    {
        // Definimos una cadena y le asignamos la cadena de conexión definida en el archivo Web.config
        string cadena = System.Configuration.ConfigurationManager.ConnectionStrings["VentasConnectionString"].ConnectionString;
        /* Creamos un objeto de la clase SQLConnection indicando como parámetro la cadena de conexión
        creada anteriormente */
        SqlConnection conexion = new SqlConnection(cadena);
        // Abrimos la conexión
        conexion.Open();
        // Creamos un objeto de la clase SqlCommand con los datos cargados en los controles TextBox
        SqlCommand comando = new SqlCommand("update Usuario set " + "Username ='" + txtUsuario1.Text
        + "', Email ='" + txtEmail.Text + "'where Username ='" + txtUsuario.Text + "'", conexion);
        /* Definimos una variable para capturar el dato devuelto por el método ExecuteNonQuery:
        retorna un entero y representa la cantidad de filas actualizadas en la tabla */
        int cantidad = comando.ExecuteNonQuery();
        // Si retorna 1 significa que el cliente existe
        if (cantidad == 1)
            lblMensaje1.Text = "Datos Modificados exitosamente. ";
        else
            lblMensaje1.Text = "No existe el Usuario indicado. ";
        // Cerramos la conexión
        conexion.Close();
    }
}