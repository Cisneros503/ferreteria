﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ferreteria/maestras/Vendedor.master" AutoEventWireup="true" CodeFile="BuscarProductos.aspx.cs" Inherits="Ferreteria_forms_vendedor_BuscarProductos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            height: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <br/>
    <br/>
    <center>
        <table>
            <tr>
                <td colspan="3">
                    <center><h4>Id de compra: #
                        <% if (Session["id_Venta"] != null)
                        {
                            Response.Write(Session["id_Venta"].ToString());
                        }
                        else
                            Response.Redirect("InicioVentas.aspx");
                        %>
                    </h4></center>

                </td>

            </tr>
            <tr>
                <td><asp:Button ID="btnCat" runat="server" Text="Buscar por Categoria" OnClick="btnCat_Click"></asp:Button></td>
                
                <td colspan="2"><asp:DropDownList ID="dropcate" Visible="false" runat="server" OnSelectedIndexChanged="dropcate_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></td>
                
            </tr>
            <tr>
                <td colspan="3"><center><asp:GridView ID="gridBusca" runat="server" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical" OnSelectedIndexChanging="gridBusca_SelectedIndexChanging">
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                    <Columns>
                        <asp:CommandField HeaderText="Acción" ShowHeader="True" ShowSelectButton="True" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                    </asp:GridView></center></td>
            </tr>
            <tr>
                <td>Buscar Producto Por Codigo: </td>
                <td><asp:Textbox ID="txtcod" runat="server" placeholder="Codigo"></asp:Textbox></td>
                <td><asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click"></asp:Button></td>
            </tr>
            <tr>

                <td align="center" colspan="3"> 
                    <center><H3>Descripcion del Producto:</H3>
                    <asp:Image ID="imgbus" runat="server" Height="200px" Width="200px"></asp:Image></td>
            </tr>

            <tr>
                <td  colspan="3"><center> <h4>--- Existencia: 
                    <% if (this.stk != null)
                        {
                            Response.Write(this.stk.ToString());
                        }
                        else
                            Response.Write(this.stk.ToString());
                   %>
                    </H4></center><asp:GridView ID="gridinfo" runat="server" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical" OnSelectedIndexChanged="gridinfo_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                    <Columns>
                        <asp:ButtonField CommandName="Select" HeaderText="Acción" ShowHeader="True" Text="agregar" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                    </asp:GridView></td>
            </tr>
            <tr>
                
                <td><asp:Label ID="lblres" runat="server"></asp:Label></td>
            </tr>
        </table>
    </center>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
</asp:Content>

