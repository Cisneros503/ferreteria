﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Ferreteria_maestra_Gerente : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.AppendHeader("Cache-Control", "no-store");
    }

    protected void lkbaddProduct_Click(object sender, EventArgs e)
    {
        Response.Redirect("AddProducto.aspx");
    }

    protected void lkbeditprod_Click(object sender, EventArgs e)
    {
        Response.Redirect("EditProduct.aspx");
    }

    protected void lkbcategoria_Click(object sender, EventArgs e)
    {
        Response.Redirect("Categorias.aspx");
    }

    protected void lkbadduser_Click(object sender, EventArgs e)
    {
        Response.Redirect("IngresarUsuario.aspx");
    }

    protected void lkbshearuser_Click(object sender, EventArgs e)
    {
        Response.Redirect("ConsultarUsuario.aspx");
    }

    protected void lkbupdateuser_Click(object sender, EventArgs e)
    {
        Response.Redirect("ModificarUsuario.aspx");
    }

    protected void lkbaddemploye_Click(object sender, EventArgs e)
    {
        Response.Redirect("AgregarEmpleado.aspx");
    }

    protected void lkbshearemploye_Click(object sender, EventArgs e)
    {
        Response.Redirect("ConsultarEmpleado.aspx");
    }

    protected void lkbupdateemploye_Click(object sender, EventArgs e)
    {
        Response.Redirect("ModificarEmpleado.aspx");
    }

    protected void lkbaddrol_Click(object sender, EventArgs e)
    {
        Response.Redirect("AgregarRol.aspx");
    }

    protected void lkbshearrol_Click(object sender, EventArgs e)
    {
        Response.Redirect("ConsultarRol.aspx");
    }

    protected void lkbupdaterol_Click(object sender, EventArgs e)
    {
        Response.Redirect("ModificarRol.aspx");
    }

    protected void lkbsalir_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Session.Clear();
        Response.Redirect("Login.aspx");
    }
}
