﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Ferreteria_forms_gerente_Proveedor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {    
        var prvLQ = new VentasDataContext();
        gvProveedores.DataSource = prvLQ.sp_listProveedores();
        gvProveedores.DataBind();
        //this.gvProveedores.Columns[0].Visible = false;       
        //gvProveedores.Visible = true;
    }

    public void ver_Proveedor(int codigo)
    {
        var prvLQ = new VentasDataContext();
        gvProveedores.DataSource = prvLQ.sp_verProveedor(codigo);
        gvProveedores.DataBind();
    }
    public void borrar_Proveedor(int codigo)
    {
        var prvLQ = new VentasDataContext();
        gvProveedores.DataSource = prvLQ.sp_DeleteProveedor(codigo);
        gvProveedores.DataBind();
    }
    public void edit_Proveedor(int codigo, string nomPr, string marcaPr, string dir, string tel, string razon)
    {
        var prvLQ = new VentasDataContext();
        gvProveedores.DataSource = prvLQ.sp_modifProveedor(codigo, nomPr,marcaPr,dir,tel,razon);
        gvProveedores.DataBind();
    }






}