﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Ferreteria_forms_gerente_AgregarEmpleado : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void BtnIngresar_Click(object sender, EventArgs e)
    {
        bool estado = true;
        // Definimos una cadena y le asignamos la cadena de conexión definida en el archivo Web.config
        string cadena = System.Configuration.ConfigurationManager.ConnectionStrings["VentasConnectionString1"].ConnectionString;
        /* Creamos un objeto de la clase SQLConnection indicando como parámetro la cadena de conexión
         creada anteriormente */
        SqlConnection conexion = new SqlConnection(cadena);
        // Abrimos la conexión
        conexion.Open();
        /* Creamos un objeto de la clase SqlCommand con los datos cargados en los controles TextBox y
        DropDownList */        
        SqlCommand comando = new SqlCommand("insert into Empleado(nomb_emp, apel_emp, tel_emp,dir_emp,DUI_emp, estado) " + "values ('" + txtnombre.Text + "','" + txtapellido.Text + "','" + txttelefono.Text + "','" + txtdireccion.Text + "','" + txtdui.Text + "', '"+estado+"')", conexion);
        // Le indicamos a SQL Server que ejecute el comando especificado anteriormente
        comando.ExecuteNonQuery();
        // Mostramos un mensaje si todo se realiza correctamente.
        lblMensaje.Text = "Se registró exitosamente el empleado.";
        // Cerramos la conexión
        conexion.Close(); 
    }
}