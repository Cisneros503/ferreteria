USE [master]
GO
/****** Object:  Database [Ventas]    Script Date: 7/4/2018 20:54:06 ******/
CREATE DATABASE [Ventas]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Ventas', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Ventas.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Ventas_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Ventas_log.ldf' , SIZE = 1088KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Ventas] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Ventas].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Ventas] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Ventas] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Ventas] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Ventas] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Ventas] SET ARITHABORT OFF 
GO
ALTER DATABASE [Ventas] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [Ventas] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Ventas] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Ventas] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Ventas] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Ventas] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Ventas] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Ventas] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Ventas] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Ventas] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Ventas] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Ventas] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Ventas] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Ventas] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Ventas] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Ventas] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Ventas] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Ventas] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Ventas] SET  MULTI_USER 
GO
ALTER DATABASE [Ventas] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Ventas] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Ventas] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Ventas] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Ventas] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Ventas]
GO
/****** Object:  Table [dbo].[Categorias]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Categorias](
	[codi_cate] [int] IDENTITY(1,1) NOT NULL,
	[nomb_cate] [varchar](45) NOT NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK__Categori__89327D5CD4F45282] PRIMARY KEY CLUSTERED 
(
	[codi_cate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ccf]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ccf](
	[codi_ccf] [int] IDENTITY(1,1) NOT NULL,
	[num_ccf] [varchar](10) NULL,
	[idEncabezado] [int] NULL,
	[giro] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[codi_ccf] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Clientes](
	[codi_clien] [int] IDENTITY(1,1) NOT NULL,
	[nomb_clien] [varchar](75) NOT NULL,
	[apel_clien] [varchar](75) NOT NULL,
	[dir_clien] [varchar](120) NOT NULL,
	[tel_clien] [varchar](15) NOT NULL,
	[DUI_clien] [varchar](10) NOT NULL,
	[NIT_clien] [varchar](17) NOT NULL,
	[cred_clien] [char](1) NOT NULL,
	[dias_cred] [int] NOT NULL,
	[id_usua] [int] NOT NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK__Clientes__326A11670E318D6B] PRIMARY KEY CLUSTERED 
(
	[codi_clien] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetalleFactura]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleFactura](
	[id_deta] [int] IDENTITY(1,1) NOT NULL,
	[idFactura] [int] NOT NULL,
	[cant_prod] [int] NOT NULL,
	[codi_prod] [int] NOT NULL,
	[descripcion] [nvarchar](50) NULL,
	[precio_unit] [money] NULL,
	[totalProduc] [money] NULL,
	[fechaRegistro] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_deta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Empleado]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Empleado](
	[codi_emp] [int] IDENTITY(1,1) NOT NULL,
	[nomb_emp] [varchar](75) NOT NULL,
	[apel_emp] [varchar](75) NOT NULL,
	[tel_emp] [varchar](15) NOT NULL,
	[dir_emp] [varchar](120) NOT NULL,
	[DUI_emp] [varchar](10) NOT NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK__Empleado__43311B5274B6936F] PRIMARY KEY CLUSTERED 
(
	[codi_emp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EncabezadoFactura]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EncabezadoFactura](
	[idEncabezado] [int] NOT NULL,
	[nombreEmpresa] [nvarchar](50) NULL,
	[logo] [image] NULL,
	[nume_fact] [varchar](10) NULL,
	[direccEmpresa] [nvarchar](100) NULL,
	[giroEmpresa] [nvarchar](100) NULL,
	[telEmpresa] [nvarchar](10) NULL,
	[nit] [nvarchar](20) NULL,
	[nrc] [nvarchar](20) NULL,
	[fechaRegistro] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[idEncabezado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Formas de Pago]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Formas de Pago](
	[form_pago] [int] IDENTITY(1,1) NOT NULL,
	[nomb_form] [varchar](45) NOT NULL,
	[clase_pago] [char](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[form_pago] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ListaClienteR]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ListaClienteR](
	[codi_clien] [int] IDENTITY(1,1) NOT NULL,
	[nomb_clien] [varchar](75) NOT NULL,
	[apel_clien] [varchar](75) NOT NULL,
	[dir_clien] [varchar](120) NOT NULL,
	[tel_clien] [varchar](15) NOT NULL,
	[DUI_clien] [varchar](10) NOT NULL,
	[NIT_clien] [varchar](17) NOT NULL,
	[cred_clien] [char](1) NOT NULL,
	[dias_cred] [int] NULL,
	[id_usua] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[codi_clien] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[listadoFactura]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[listadoFactura](
	[idFactura] [int] IDENTITY(1,1) NOT NULL,
	[NumFactu] [int] NULL,
	[fechaFactura] [date] NULL,
	[subtotal] [float] NULL,
	[iva] [float] NULL,
	[total] [float] NULL,
	[codi_clien] [int] NULL,
	[codi_emp] [int] NULL,
	[form_pago] [int] NULL,
	[tipoFactura] [int] NULL,
	[fechaRegistro] [date] NULL,
	[estado] [bit] NULL,
	[idEncabezado] [int] NULL,
 CONSTRAINT [PK__listadoF__3CD5687E04BBB521] PRIMARY KEY CLUSTERED 
(
	[idFactura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ListaFacturaR]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListaFacturaR](
	[idFactura] [int] IDENTITY(1,1) NOT NULL,
	[fechaFactura] [date] NULL,
	[subtotal] [float] NULL,
	[iva] [float] NULL,
	[total] [float] NULL,
	[codi_clien] [int] NULL,
	[codi_emp] [int] NULL,
	[form_pago] [int] NULL,
	[tipoFactura] [int] NULL,
	[fechaRegistro] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[idFactura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Productos]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Productos](
	[codi_prod] [int] IDENTITY(1,1) NOT NULL,
	[codi_cate] [int] NOT NULL,
	[nomb_prod] [varchar](125) NOT NULL,
	[estado] [varchar](15) NOT NULL,
	[tipo_prod] [varchar](20) NOT NULL,
	[descripcion] [varchar](150) NOT NULL,
	[Imagen] [varchar](2500) NOT NULL,
	[codi_prov] [int] NOT NULL,
	[Precio] [decimal](8, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[codi_prod] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Proveedor]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Proveedor](
	[codi_prov] [int] IDENTITY(1,1) NOT NULL,
	[nomb_prov] [varchar](75) NOT NULL,
	[marca_prov] [varchar](75) NOT NULL,
	[dir_prov] [varchar](120) NOT NULL,
	[tel_prov] [varchar](10) NOT NULL,
	[razon_social_prov] [varchar](75) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[codi_prov] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Referencias]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Referencias](
	[codi_ref] [int] IDENTITY(1,1) NOT NULL,
	[nomb_ref] [varchar](75) NOT NULL,
	[dir_ref] [varchar](120) NOT NULL,
	[tel_ref] [varchar](15) NOT NULL,
	[codi_clien] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[codi_ref] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Roles](
	[codi_rol] [int] IDENTITY(1,1) NOT NULL,
	[nomb_rol] [varchar](75) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[codi_rol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Stock]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Stock](
	[id_stock] [int] IDENTITY(1,1) NOT NULL,
	[total_prod_stock] [numeric](12, 5) NOT NULL,
	[fecha_stock] [date] NULL,
	[actualiz_prod_stock] [numeric](12, 5) NOT NULL,
	[codi_prod] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_stock] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tipo Cobro]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tipo Cobro](
	[id_cobro] [int] IDENTITY(1,1) NOT NULL,
	[form_pago] [int] NULL,
	[fech_cobr] [datetime] NULL,
	[fech_regi] [datetime] NULL,
	[mont_cobr] [money] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_cobro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tipo Documento]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tipo Documento](
	[codi_docu] [int] IDENTITY(1,1) NOT NULL,
	[nomb_docu] [varchar](75) NOT NULL,
	[form_pago] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[codi_docu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tipo Movimiento]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tipo Movimiento](
	[codi_mov] [int] IDENTITY(1,1) NOT NULL,
	[codi_docu] [int] NOT NULL,
	[efecto] [char](1) NULL,
PRIMARY KEY CLUSTERED 
(
	[codi_mov] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[id_usua] [int] IDENTITY(1,1) NOT NULL,
	[login] [varchar](15) NOT NULL,
	[password] [varbinary](500) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[estado] [bit] NULL,
	[fecha] [date] NOT NULL,
	[codi_emp] [int] NULL,
	[codi_rol] [int] NULL,
 CONSTRAINT [PK__Usuario__D2D1C434478DDE85] PRIMARY KEY CLUSTERED 
(
	[id_usua] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[DetalleFactura] ADD  DEFAULT (getdate()) FOR [fechaRegistro]
GO
ALTER TABLE [dbo].[EncabezadoFactura] ADD  DEFAULT (getdate()) FOR [fechaRegistro]
GO
ALTER TABLE [dbo].[listadoFactura] ADD  CONSTRAINT [DF__listadoFa__fecha__37A5467C]  DEFAULT (getdate()) FOR [fechaRegistro]
GO
ALTER TABLE [dbo].[ListaFacturaR] ADD  DEFAULT (getdate()) FOR [fechaRegistro]
GO
ALTER TABLE [dbo].[tipo Cobro] ADD  DEFAULT (getdate()) FOR [fech_cobr]
GO
ALTER TABLE [dbo].[tipo Cobro] ADD  DEFAULT (getdate()) FOR [fech_regi]
GO
ALTER TABLE [dbo].[Usuario] ADD  CONSTRAINT [DF_Usuario_fecha]  DEFAULT (getdate()) FOR [fecha]
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD  CONSTRAINT [FK__Clientes__id_usu__1A14E395] FOREIGN KEY([id_usua])
REFERENCES [dbo].[Usuario] ([id_usua])
GO
ALTER TABLE [dbo].[Clientes] CHECK CONSTRAINT [FK__Clientes__id_usu__1A14E395]
GO
ALTER TABLE [dbo].[DetalleFactura]  WITH CHECK ADD  CONSTRAINT [FK__DetalleFa__idFac__3A81B327] FOREIGN KEY([idFactura])
REFERENCES [dbo].[listadoFactura] ([idFactura])
GO
ALTER TABLE [dbo].[DetalleFactura] CHECK CONSTRAINT [FK__DetalleFa__idFac__3A81B327]
GO
ALTER TABLE [dbo].[DetalleFactura]  WITH CHECK ADD  CONSTRAINT [fk_productoscodiprod] FOREIGN KEY([codi_prod])
REFERENCES [dbo].[Productos] ([codi_prod])
GO
ALTER TABLE [dbo].[DetalleFactura] CHECK CONSTRAINT [fk_productoscodiprod]
GO
ALTER TABLE [dbo].[listadoFactura]  WITH CHECK ADD  CONSTRAINT [FK__listadoFa__codi___34C8D9D1] FOREIGN KEY([codi_clien])
REFERENCES [dbo].[Clientes] ([codi_clien])
GO
ALTER TABLE [dbo].[listadoFactura] CHECK CONSTRAINT [FK__listadoFa__codi___34C8D9D1]
GO
ALTER TABLE [dbo].[listadoFactura]  WITH CHECK ADD  CONSTRAINT [FK__listadoFa__codi___35BCFE0A] FOREIGN KEY([codi_emp])
REFERENCES [dbo].[Empleado] ([codi_emp])
GO
ALTER TABLE [dbo].[listadoFactura] CHECK CONSTRAINT [FK__listadoFa__codi___35BCFE0A]
GO
ALTER TABLE [dbo].[listadoFactura]  WITH CHECK ADD  CONSTRAINT [FK__listadoFa__form___36B12243] FOREIGN KEY([form_pago])
REFERENCES [dbo].[Formas de Pago] ([form_pago])
GO
ALTER TABLE [dbo].[listadoFactura] CHECK CONSTRAINT [FK__listadoFa__form___36B12243]
GO
ALTER TABLE [dbo].[listadoFactura]  WITH CHECK ADD  CONSTRAINT [FK__listadoFa__idEnc__403A8C7D] FOREIGN KEY([idEncabezado])
REFERENCES [dbo].[EncabezadoFactura] ([idEncabezado])
GO
ALTER TABLE [dbo].[listadoFactura] CHECK CONSTRAINT [FK__listadoFa__idEnc__403A8C7D]
GO
ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [fk_categoriascodicate] FOREIGN KEY([codi_cate])
REFERENCES [dbo].[Categorias] ([codi_cate])
GO
ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [fk_categoriascodicate]
GO
ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [fk_proveedorcodiprov] FOREIGN KEY([codi_prov])
REFERENCES [dbo].[Proveedor] ([codi_prov])
GO
ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [fk_proveedorcodiprov]
GO
ALTER TABLE [dbo].[Referencias]  WITH CHECK ADD  CONSTRAINT [FK__Referenci__codi___1CF15040] FOREIGN KEY([codi_clien])
REFERENCES [dbo].[Clientes] ([codi_clien])
GO
ALTER TABLE [dbo].[Referencias] CHECK CONSTRAINT [FK__Referenci__codi___1CF15040]
GO
ALTER TABLE [dbo].[Stock]  WITH CHECK ADD FOREIGN KEY([codi_prod])
REFERENCES [dbo].[Productos] ([codi_prod])
GO
ALTER TABLE [dbo].[tipo Cobro]  WITH CHECK ADD  CONSTRAINT [fkformasdepagoformpago] FOREIGN KEY([form_pago])
REFERENCES [dbo].[Formas de Pago] ([form_pago])
GO
ALTER TABLE [dbo].[tipo Cobro] CHECK CONSTRAINT [fkformasdepagoformpago]
GO
ALTER TABLE [dbo].[Tipo Documento]  WITH CHECK ADD FOREIGN KEY([form_pago])
REFERENCES [dbo].[Formas de Pago] ([form_pago])
GO
ALTER TABLE [dbo].[Tipo Movimiento]  WITH CHECK ADD  CONSTRAINT [fktipodocumentocodidocu] FOREIGN KEY([codi_docu])
REFERENCES [dbo].[Tipo Documento] ([codi_docu])
GO
ALTER TABLE [dbo].[Tipo Movimiento] CHECK CONSTRAINT [fktipodocumentocodidocu]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [fk_empleadocodiemp] FOREIGN KEY([codi_emp])
REFERENCES [dbo].[Empleado] ([codi_emp])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [fk_empleadocodiemp]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [fk_rolescodirol] FOREIGN KEY([codi_rol])
REFERENCES [dbo].[Roles] ([codi_rol])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [fk_rolescodirol]
GO
/****** Object:  StoredProcedure [dbo].[Listprodxcat]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[Listprodxcat]
@codicate int
as
begin
select codi_prod, nomb_prod, estado, tipo_prod, descripcion, Precio, Imagen from Productos where codi_cate=@codicate
end
GO
/****** Object:  StoredProcedure [dbo].[ps_BuscarProductos]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[ps_BuscarProductos]
@codi_cate int
as
begin
select *from Productos where codi_cate=@codi_cate
end

GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarCategoria]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_AgregarCategoria]
@nom_cate varchar(45),
@estado bit
as
begin
insert into Categorias values(@nom_cate, @estado)
end


GO
/****** Object:  StoredProcedure [dbo].[SP_AgregarClientes]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_AgregarClientes]
@codi_clien int,
@nomb_clien varchar(75),
@apel_clien varchar(75),
@DUI_clien varchar(10),
@NIT_clien varchar(17),
@dir_clien varchar (120),
@tel_clien varchar (15),
@dias_cred int,
@cred_clien char (1)
as 
begin
     insert into Clientes (codi_clien,nomb_clien,apel_clien,DUI_clien,NIT_clien,dir_clien,tel_clien,dias_cred,cred_clien) 
	 values
	 (@codi_clien,@nomb_clien,@apel_clien,@DUI_clien,@NIT_clien,@dir_clien,@tel_clien,@dias_cred,@cred_clien)
	 end


GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarEmpleado]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_AgregarEmpleado]
  @nomb_emp varchar(75), 
  @apel_emp varchar(75), 
  @tel_emp varchar(15),
  @dir_emp varchar(120),
  @dui_emp varchar(10),
  @estado char(1)
 as
 begin
  insert into Empleado(nomb_emp,apel_emp,tel_emp,dir_emp,DUI_emp,estado) values(@nomb_emp,@apel_emp,@tel_emp,@dir_emp,@dui_emp,@estado);
 end;


GO
/****** Object:  StoredProcedure [dbo].[SP_AgregarFormasDePago]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_AgregarFormasDePago]
@nomb_form varchar (45),
@clase_pago char (1)
AS
BEGIN
	INSERT INTO [Formas de Pago] (nomb_form, clase_pago)
	VALUES (@nomb_form, @clase_pago)
END


GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarProductos]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_AgregarProductos]
@codi_cate int,
@nom_prod varchar(125),
@estado varchar(15),
@tipo_prod varchar(20),
@descripcion varchar(150),
@imagen varchar(2500),
@Codi_prov int,
@precio decimal(8,2)
as
begin
insert into Productos values(@codi_cate, @nom_prod, @estado, @tipo_prod, @descripcion, @imagen, @Codi_prov, @precio)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AgregarReferencia]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_AgregarReferencia] 
  @codi_ref int,
  @nomb_ref varchar (75),
  @dir_ref varchar(120),
  @tel_ref varchar(15)
  as 
  begin 
  insert into Referencias (nomb_ref,dir_ref,tel_ref)
  values 
  (@nomb_ref,@dir_ref,@tel_ref)
  end 


GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarRol]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 create procedure [dbo].[sp_AgregarRol]
@nomb_rol varchar(75)
as
begin
insert into Roles(nomb_rol) values(@nomb_rol)
end


GO
/****** Object:  StoredProcedure [dbo].[sp_agregarStocks]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Procedure [dbo].[sp_agregarStocks]
@total_prod numeric(12,5),
@fecha_stock date,
@actualizProd numeric(12,5),
@codi_prod int
as
begin
insert into Stock values(@total_prod, @fecha_stock, @actualizProd, @codi_prod)
end



GO
/****** Object:  StoredProcedure [dbo].[SP_AgregarTipoCobro]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_AgregarTipoCobro]
@form_pago int,
@mont_cobr money
AS
BEGIN
	INSERT INTO [tipo Cobro] (form_pago, mont_cobr)
	VALUES (@form_pago, @mont_cobr)
END


GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarUsuario]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_AgregarUsuario]
@Login  varchar(15),
@Password varchar(15),
@Email varchar(50),
@Estado bit,
@Patron varchar(25)
as
 begin
     insert into Usuario
	 (login,password,email,estado)
	 values
	 (@Login,
	 encryptbypassphrase (@patron,@Password),
	 @Email,@Estado)
	 select @@IDENTITY 
 end



GO
/****** Object:  StoredProcedure [dbo].[sp_BuscaCategoria]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_BuscaCategoria]
@nom_cate varchar(45)
as
begin
select *from Categorias where nomb_cate like @nom_cate+'%'
end


GO
/****** Object:  StoredProcedure [dbo].[SP_BuscarClientes]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 create procedure [dbo].[SP_BuscarClientes] 
 @nomb_clien varchar (75)
 as 
 begin
 select * from Clientes where nomb_clien like @nomb_clien+'%'
 end 


GO
/****** Object:  StoredProcedure [dbo].[sp_BuscarStocks]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Procedure [dbo].[sp_BuscarStocks]
@fecha_stock date
as
begin
select *from Stock where year(fecha_stock)=year(@fecha_stock)
end


GO
/****** Object:  StoredProcedure [dbo].[SP_ConsultarClientes]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_ConsultarClientes]
@codi_clien int
as 
 begin 
select * from Clientes
where codi_clien= @codi_clien
end  


GO
/****** Object:  StoredProcedure [dbo].[SP_ConsultarEmpleado]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_ConsultarEmpleado]
@nomb_emp varchar(75)
as
begin
    
	   select nomb_emp,apel_emp,tel_emp,dir_emp,DUI_emp,estado from Empleado
	   where nomb_emp = @nomb_emp
	
end


GO
/****** Object:  StoredProcedure [dbo].[SP_ConsultarRol]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_ConsultarRol]
@codi_rol int
as
begin
    
	   select nomb_rol from Roles
	   where codi_rol = @codi_rol
	
end


GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarUsuario]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE procedure [dbo].[sp_ConsultarUsuario]
@Login varchar (15),
@Patron varchar (15)
as
begin
    
	   select login,
	   Convert(varchar(15),DECRYPTBYPASSPHRASE(@Patron,password)) password,email,fecha,estado from Usuario
	   where login = @Login
	
end



GO
/****** Object:  StoredProcedure [dbo].[sp_crearFactura]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_crearFactura](@NumFactu int,@fechaFactura date,@subtotal float,@iva float,@total float,@codi_clien int,@codi_emp int,@tipoFactura int,
	@tipoPago int,@estado int,@cant_prod int,@codi_prod int,@descripcion nvarchar(50),@precio_unit money,@totalProduct money)
 as
 begin
	declare @result int
	set @result =(select NumFactu from listadoFactura where NumFactu=@NumFactu)
	if(@NumFactu = @result)
	begin
		update listadoFactura set subtotal=@subtotal,iva=@iva,total=@total where NumFactu=@NumFactu

		insert into DetalleFactura(idFactura,cant_prod,codi_prod,descripcion,precio_unit,totalProduc)
		values(@NumFactu,@cant_prod,@codi_prod,@descripcion,@precio_unit,@totalProduct)
	end
	else
	begin
		insert into listadoFactura(NumFactu,fechaFactura,subtotal,iva,total,codi_clien,codi_emp,tipoFactura,estado,idEncabezado)
		values(@NumFactu,@fechaFactura,@subtotal,@iva,@total,@codi_clien,@codi_emp,@tipoFactura,@estado,1)

		insert into DetalleFactura(idFactura,cant_prod,codi_prod,descripcion,precio_unit,totalProduc)
		values(@NumFactu,@cant_prod,@codi_prod,@descripcion,@precio_unit,@totalProduct)
	end

 end


GO
/****** Object:  StoredProcedure [dbo].[sp_detalleDeFactura]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_detalleDeFactura]
as
begin
	select * from listadoFactura
	select * from DetalleFactura
end


GO
/****** Object:  StoredProcedure [dbo].[sp_EditarCategoria]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_EditarCategoria]
@codi_cate int,
@nom_cate varchar(45),
@estado bit
as
begin
update Categorias set nomb_cate=@nom_cate, estado=@estado where codi_cate=@codi_cate
end


GO
/****** Object:  StoredProcedure [dbo].[sp_editarStocks]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Procedure [dbo].[sp_editarStocks]
@total_prod numeric(12,5),
@fecha_stock date,
@actualizProd numeric(12,5),
@codi_prod int,
@idstock int
as
begin
update Stock set total_prod_stock=@total_prod, fecha_stock=@fecha_stock, actualiz_prod_stock=@actualizProd, codi_prod=@codi_prod where id_stock=@idstock
end



GO
/****** Object:  StoredProcedure [dbo].[sp_eliminarDetalleFactura]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 create procedure [dbo].[sp_eliminarDetalleFactura](@NumFactu int,@codi_prod int)
 as
 begin
	delete DetalleFactura where idFactura = @NumFactu and codi_prod = @codi_prod
 end


GO
/****** Object:  StoredProcedure [dbo].[sp_eliminarFactura]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_eliminarFactura](@NumFactu int)
 as
 begin
	update listadoFactura set estado = 0 where NumFactu=@NumFactu
 end


GO
/****** Object:  StoredProcedure [dbo].[sp_encabezadoFactura]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_encabezadoFactura]
AS
begin
SELECT * from EncabezadoFactura
end


GO
/****** Object:  StoredProcedure [dbo].[sp_InsertProveedor]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_InsertProveedor] 
@nomPr varchar(75),@marcaPr varchar (75), @dir varchar(120),  @tel varchar (15) ,  @razon varchar(10) 
as
begin  
 Insert into dbo.proveedor(nomb_prov, marca_prov, dir_prov, tel_prov, razon_social_prov)
 Values (@nomPr, @marcaPr, @dir, @tel,  @razon)
return 0
end


GO
/****** Object:  StoredProcedure [dbo].[sp_ListarCategoria]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_ListarCategoria]
as
begin
select *from Categorias where estado=1
end


GO
/****** Object:  StoredProcedure [dbo].[sp_ListarProductos]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Listar Productos--
CREATE Procedure [dbo].[sp_ListarProductos]
as
begin
select codi_prod, nomb_prod, estado, descripcion, Precio from Productos
end
GO
/****** Object:  StoredProcedure [dbo].[sp_ListarStocks]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[sp_ListarStocks]
as
begin
select *from Stock
end



GO
/****** Object:  StoredProcedure [dbo].[sp_listProveedores]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[sp_listProveedores]
as
begin
select *from Proveedor
end


GO
/****** Object:  StoredProcedure [dbo].[SP_ModificarCliente]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ModificarCliente]
@codi_clien int,
@nomb_clien varchar(75),
@apel_clien varchar(75),
@DUI_clien varchar(10),
@NIT_clien varchar(17),
@dir_clien varchar (120),
@tel_clien varchar (15),
@dias_cred int,
@cred_clien char (1)
as 
  begin 
  update Clientes
  set  
  nomb_clien = @nomb_clien, apel_clien=@apel_clien, DUI_clien=@DUI_clien, NIT_clien=@NIT_clien, dir_clien=@dir_clien,tel_clien=@tel_clien,dias_cred=@dias_cred, cred_clien=@cred_clien
  where codi_clien=@codi_clien
  SELECT 'Registro modificado satisfactoriamente'
  end 


GO
/****** Object:  StoredProcedure [dbo].[sp_ModificarDetalleFactura]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ModificarDetalleFactura](@NumFactu int,@cant_prod int,@codi_prod int,@descripcion nvarchar(50),@precio_unit money,@totalProduc money)
 as
 begin
	update DetalleFactura set cant_prod=@cant_prod,@codi_prod=@codi_prod,descripcion=@descripcion,precio_unit=@precio_unit,totalProduc=@totalProduc where idFactura = @NumFactu and codi_prod = @codi_prod
 end


GO
/****** Object:  StoredProcedure [dbo].[sp_ModificarEmpleado]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_ModificarEmpleado]  
  @nomb_emp varchar(75), 
  @apel_emp varchar(75), 
  @tel_emp varchar(15),
  @dir_emp varchar(120),
  @dui_emp varchar(10),
  @estado char(1)
as
 begin
     update  Empleado
	 set nomb_emp = @nomb_emp,apel_emp=@apel_emp,
	 tel_emp = @tel_emp,
	 dir_emp = @dir_emp,DUI_emp = @dui_emp,estado = @estado
	 where nomb_emp = @nomb_emp
	 select 'Registro modificado satisfactoriamente'
 end


GO
/****** Object:  StoredProcedure [dbo].[SP_ModificarFormasDePago]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_ModificarFormasDePago]
@form_pago int,
@nomb_form varchar (45),
@clase_pago char (1)
AS
BEGIN
	UPDATE [Formas de Pago]
	SET nomb_form = @nomb_form, clase_pago = @clase_pago
	WHERE form_pago = @form_pago
END


GO
/****** Object:  StoredProcedure [dbo].[sp_modificarProductos]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modificae Productos--
CREATE Procedure [dbo].[sp_modificarProductos]
@codi_cate int,
@nom_prod varchar(125),
@estado varchar(15),
@tipo_prod varchar(20),
@descripcion varchar(150),
@imagen varchar(2500),
@Codi_prov int,
@precio decimal(8,2),
@codi_prod int
as
begin
Update Productos set codi_cate=@codi_cate, nomb_prod=@nom_prod, estado=@estado, tipo_prod=@tipo_prod, descripcion=@descripcion, Imagen=@imagen, Codi_prov=@Codi_prov, Precio=@precio where codi_prod=@codi_prod
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ModificarReferencias]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_ModificarReferencias] 
  @codi_ref int,
  @nomb_ref varchar (75),
  @dir_ref varchar(120),
  @tel_ref varchar(15)
  as 
  begin 
  update Referencias 
  set 
  nomb_ref=@nomb_ref,dir_ref=@dir_ref,tel_ref=@tel_ref
  where codi_ref=@codi_ref
  select 'Referecia modificada satisfactoriamente'
  end 


GO
/****** Object:  StoredProcedure [dbo].[sp_ModificarRol]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ModificarRol]
@codi_rol int,
@Nomb_rol varchar(75)
as
 begin
     update  Roles
	 set nomb_rol = @Nomb_rol
	 where codi_rol = @codi_rol
	 select 'Registro modificado satisfactoriamente'
 end


GO
/****** Object:  StoredProcedure [dbo].[SP_ModificarTipoCobro]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_ModificarTipoCobro]
@id_cobro int,
@form_pago int,
@mont_cobr money
AS
BEGIN
	UPDATE [tipo Cobro]
	SET form_pago = @form_pago, mont_cobr = @mont_cobr
	WHERE id_cobro = @id_cobro
END


GO
/****** Object:  StoredProcedure [dbo].[sp_ModificarUsuario]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_ModificarUsuario]

@Login  varchar(15),
@Password varchar(15),
@Email varchar(50),
@Estado bit,
@Patron varchar(25)
as
 begin
     update  Usuario
	 set login=@Login,
	 password = encryptbypassphrase (@patron,@Password),
	 email = @Email,estado = @Estado
	 where login=@Login
	 select 'Registro modificado satisfactoriamente'
 end



GO
/****** Object:  StoredProcedure [dbo].[sp_modifProveedor]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_modifProveedor] 
@codProv int, @nomPr varchar(75),@marcaPr varchar (75), @dir varchar(120),  @tel varchar (15) ,  @razon varchar(10)
as
begin 
if exists (select * from dbo.Proveedor where Proveedor.codi_prov = @codProv)
 begin
 update dbo.Proveedor
 set
 nomb_prov = @nomPr,
 marca_prov = @marcaPr , 
 dir_prov = @dir, 
 tel_prov = @tel, 
 razon_social_prov = @razon
 where Proveedor.codi_prov = @codProv
 return 0
end
else
 begin 
 raiserror('Error -998: El registro NO existe.', 1, 10)
 return -998
 end
end


GO
/****** Object:  StoredProcedure [dbo].[SP_MostrarFormasDePago]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_MostrarFormasDePago]
@form_pago int
AS
BEGIN
	SELECT * FROM [Formas de Pago]
	WHERE form_pago = @form_pago
END


GO
/****** Object:  StoredProcedure [dbo].[SP_MostrarTipoCobro]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_MostrarTipoCobro]
@id_cobro int
AS
BEGIN
	SELECT * FROM [tipo Cobro]
	WHERE id_cobro = @id_cobro
END


GO
/****** Object:  StoredProcedure [dbo].[sp_noGuardarFactura]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_noGuardarFactura](@NumFactu int)
 as
 begin
	delete DetalleFactura where idFactura=@NumFactu
	delete listadoFactura where NumFactu=@NumFactu
 end


GO
/****** Object:  StoredProcedure [dbo].[sp_numFactura]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_numFactura](@tipoFactura int)
as
Begin
 Declare
@numFac int
 set @numFac = (select count (NumFactu) from listadoFactura where tipoFactura=@tipoFactura)+1
 select @numFac
end


GO
/****** Object:  StoredProcedure [dbo].[sp_SearchproductCodigo]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_SearchproductCodigo]
@codi_prod int
as
begin
select codi_cate, nomb_prod, tipo_prod, descripcion, Precio, Imagen from Productos where estado='Activo' and codi_prod=@codi_prod
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ValidarUsuario]    Script Date: 7/4/2018 20:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ValidarUsuario]
@Login varchar(15),
@password varchar(15),
@Patron varchar(15)
as
begin
    if exists  (select * from Usuario a where login = @Login
	   and Convert(varchar(15),DECRYPTBYPASSPHRASE(@Patron,password)) = @password)
	   select id_usua,email,fecha,codi_emp,codi_rol from Usuario
	   where login = @Login and
	   Convert(varchar(15),DECRYPTBYPASSPHRASE(@Patron,password)) = @password
	else
	    select -1 as id_usua
end



GO
USE [master]
GO
ALTER DATABASE [Ventas] SET  READ_WRITE 
GO
