﻿<%@ Page Title="" Language="C#" MasterPageFile="../../maestras/Gerente.master" AutoEventWireup="true" CodeFile="Categorias.aspx.cs" Inherits="Ferreteria_forms_gerente_Categorias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br/>
    <br/>
    <br/>
     <center>
            <table>
                <tr>
                    <td colspan="2"><h3>Agregar Categorias</h3></td>
                </tr>
                <tr>
                    <td>Nueva Categoria:</td>
                    <td><asp:textbox ID="txtcat" runat="server" placeholder="Nombre Categoria"></asp:textbox></td>
                </tr>
                <tr>
                    <td>Estado</td>
                    <td><asp:DropDownList ID="DropCat" runat="server">
                        <asp:ListItem Value="1">Activa</asp:ListItem>
                        <asp:ListItem Value="0">Inactiva</asp:ListItem>
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td><asp:button ID="btnInsertar" runat="server" Text="Agregar" OnClick="btnInsertar_Click"></asp:button></td>
                    <td><asp:button ID="btnEditar" runat="server" Text="Actualizar" OnClick="btnEditar_Click" Visible="false"></asp:button></td>
                </tr>
                <tr>
                    <td colspan="2"><asp:label ID="lblresul" runat="server"></asp:label></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="GridVcat" runat="server" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical" OnSelectedIndexChanging="GridVcat_SelectedIndexChanging">
                            <AlternatingRowStyle BackColor="#CCCCCC" />
                            <Columns>
                                <asp:CommandField HeaderText="Acción" ShowHeader="True" ShowSelectButton="True" />
                            </Columns>
                            <FooterStyle BackColor="#CCCCCC" />
                            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#808080" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#383838" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </center>
<br/>
    <br/>
    <br/>
    <br/>
    <br/>
</asp:Content>

