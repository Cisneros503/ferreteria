﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Ferreteria_forms_gerente_Categorias : System.Web.UI.Page
{
    VentasDataContext MiLinq = new VentasDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        GridVcat.DataSource = MiLinq.sp_ListarCategoria();
        GridVcat.DataBind();
    }

    protected void btnInsertar_Click(object sender, EventArgs e)
    {
        string nomcate = txtcat.Text.Trim();
        int est = Convert.ToInt32(DropCat.SelectedValue);
        bool estado;
        if (est == 1)
        {
            estado = true;
        }
        else
        {
            estado = false;
        }
        try
        {
            MiLinq.sp_AgregarCategoria(nomcate, estado);
            txtcat.Text = "";
            lblresul.Text = "Categoria Agregada Correctmente";
            lblresul.ForeColor = System.Drawing.Color.DarkGreen;
            GridVcat.DataSource = MiLinq.sp_ListarCategoria();
            GridVcat.DataBind();
        }
        catch
        {
            txtcat.Text = "";
            lblresul.Text = "Categoria No Agregada";
            lblresul.ForeColor = System.Drawing.Color.Crimson;
        }
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        string nnombre = txtcat.Text.Trim();
        int id = Convert.ToInt32(Session["idcat"].ToString());
        int est = Convert.ToInt32(DropCat.SelectedValue);
        bool estado;
        if (est == 1)
        {
            estado = true;
        }
        else
        {
            estado = false;
        }
        try
        {
            MiLinq.sp_EditarCategoria(id, nnombre, estado);
            txtcat.Text = "";
            lblresul.Text = "Categoria Actualizada Correctamente";
            lblresul.ForeColor = System.Drawing.Color.DarkGreen;
            GridVcat.DataSource = MiLinq.sp_ListarCategoria();
            GridVcat.DataBind();
        }
        catch
        {
            txtcat.Text = "";
            lblresul.Text = "Categoria No se Actualizo";
            lblresul.ForeColor = System.Drawing.Color.Crimson;
        }
    }

    protected void GridVcat_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Session["idcat"] = GridVcat.Rows[e.NewSelectedIndex].Cells[1].Text;
        txtcat.Text = GridVcat.Rows[e.NewSelectedIndex].Cells[2].Text;
        btnEditar.Visible = true;
    }
}