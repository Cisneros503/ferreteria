﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Ferreteria_forms_vendedor_BuscarProductos : System.Web.UI.Page
{
    VentasDataContext MiLinq = new VentasDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //dropcate.DataSource = obj2.sp_ListarCategoria();
            //dropcate.DataTextField = "nomb_cate";
            //dropcate.DataValueField = "codi_cate";
            //dropcate.DataBind();
            //gridBusca.DataSource = obj.sp_ListarProductos();
            //gridBusca.DataBind();
        }
                           
            
        
    }
    public int stk;
    public int cod;
    protected void btnBuscar_Click(object sender, EventArgs e)
    {       
        try
        {
            string cod = txtcod.Text.Trim();
            int codi = Convert.ToInt32(cod);
            this.cod = codi;
            gridinfo.DataSource = MiLinq.sp_SearchproductCodigo(codi);
            gridinfo.DataBind();
            this.stk = Convert.ToInt32(MiLinq.sp_VerStockId(codi));
            string img = gridinfo.Rows[0].Cells[6].Text;
            imgbus.ImageUrl = "~/IMGs/" + img;
            lblres.Text = "";
        }
        catch(Exception ex)
        {
            lblres.Text = "Producto no existe o esta inactivo";
            lblres.ForeColor = System.Drawing.Color.Crimson;
        }
        

        
    }

    protected void gridBusca_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int cod = Convert.ToInt32(gridBusca.Rows[e.NewSelectedIndex].Cells[1].Text);
        this.cod = cod;
        try
        {
            gridinfo.DataSource = MiLinq.sp_SearchproductCodigo(cod);
            gridinfo.DataBind();
            this.stk = Convert.ToInt32(MiLinq.sp_VerStockId(cod));
            string img = gridinfo.Rows[0].Cells[6].Text;
            imgbus.ImageUrl = "~/IMGs/" + img;
            lblres.Text = "";
        }
        catch (Exception ex)
        {
            lblres.Text = "Producto no existe o esta inactivo";
            lblres.ForeColor = System.Drawing.Color.Crimson;
            //OJO CAMBIAR LA IMAGEN A LA HORA DEL ERROR QUE NO QUEDE EL PRODUCTO ANTERIOR
            //imgbus.ImageUrl = "~/IMGs/" + img; //NUEVA IMG
        }
    }


    protected void dropcate_SelectedIndexChanged(object sender, EventArgs e)
    {
        int code = Convert.ToInt32(dropcate.SelectedValue);
       try
        {
            gridBusca.DataSource = MiLinq.Listprodxcat(code);
            gridBusca.DataBind();
        }
        catch(Exception ex)
        {
            lblres.Text = "El Producto No existe";
            lblres.ForeColor = System.Drawing.Color.Crimson;
        }
    }

    protected void btnCat_Click(object sender, EventArgs e)
    {
        if (dropcate.Visible == false || gridBusca.Visible == false)
        {
            dropcate.Visible = true;
            dropcate.DataSource = MiLinq.sp_ListarCategoria();
            dropcate.DataTextField = "nomb_cate";
            dropcate.DataValueField = "codi_cate";
            dropcate.DataBind();
            gridBusca.DataSource = MiLinq.sp_ListarProductos();
            gridBusca.DataBind();
            gridBusca.Visible = true;
        }else if(dropcate.Visible == true || gridBusca.Visible == true)
        {
            dropcate.Visible = false;
            gridBusca.Visible = false;
        }
    }

    protected void gridinfo_SelectedIndexChanged(object sender, EventArgs e)
    {
        //int code = Convert.ToInt32(gridinfo.SelectedValue);
        //int cod = Convert.ToInt32(gridBusca.Rows[0].Cells[0].Text);
       // Response.Write(cod);
        Response.Redirect(this.cod + "+Esta es una prueba!!.aspx");
    }
}