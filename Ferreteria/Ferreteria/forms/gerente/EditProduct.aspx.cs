﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Ferreteria_forms_gerente_EditProduct : System.Web.UI.Page
{
    VentasDataContext MiLinq = new VentasDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["nPhoto"] = "";
            gridprod.DataSource = MiLinq.sp_ListarProductos();
            gridprod.DataBind();
            dropcat.DataSource = MiLinq.sp_ListarCategoria();
            dropcat.DataTextField = "nomb_cate";
            dropcat.DataValueField = "codi_cate";
            dropcat.DataBind();
            dropncat.DataSource = MiLinq.sp_ListarCategoria();
            dropncat.DataTextField = "nomb_cate";
            dropncat.DataValueField = "codi_cate";
            dropncat.DataBind();
            ndropprov.DataSource = MiLinq.sp_listProveedores();
            ndropprov.DataTextField = "nomb_prov";
            ndropprov.DataValueField = "codi_prov";
            ndropprov.DataBind();

        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        
        int codicat = Convert.ToInt32(dropcat.SelectedValue);
        try
        {
            gridprod.DataSource = MiLinq.ps_BuscarProductos(codicat);
            gridprod.DataBind();

        }
        catch
        {

        }
    }

    protected void gridprod_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        txtnnombre.Text = gridprod.Rows[e.NewSelectedIndex].Cells[3].Text;
        txtprecio.Text = gridprod.Rows[e.NewSelectedIndex].Cells[9].Text;
        txtndescrip.Text = gridprod.Rows[e.NewSelectedIndex].Cells[6].Text;
        txtntipo.Text = gridprod.Rows[e.NewSelectedIndex].Cells[5].Text;
        Session["OldPhoto"] = gridprod.Rows[e.NewSelectedIndex].Cells[7].Text;
        ncarg.ImageUrl = "~/IMGs/" + Session["OldPhoto"].ToString();
        lblid.Text = gridprod.Rows[e.NewSelectedIndex].Cells[1].Text;
        lblres.Text = Session["OldPhoto"].ToString();
    }

    protected void ncarga_Click(object sender, EventArgs e)
    {
        if (nsubir.HasFile)
        {
            if (nsubir.PostedFile != null && nsubir.PostedFile.ContentLength>0)
            {
                Session["nPhoto"] = nsubir.PostedFile.FileName;
                string exten = System.IO.Path.GetExtension(Session["nPhoto"].ToString()).ToLower();
                if (exten==".jpg" || exten==".png" || exten==".jpeg")
                {
                    if (Session["nPhoto"].ToString()==Session["OldPhoto"].ToString())
                    {
                        lblres.Text = "Ya existe una imagen con ese nombre";
                        lblres.ForeColor = System.Drawing.Color.Crimson;
                        Session["nPhoto"] = "";
                    }
                    else
                    {
                        string dir = Server.MapPath("~/IMGs/");
                        nsubir.PostedFile.SaveAs(dir+Session["nPhoto"].ToString());
                        ncarg.ImageUrl = "~/IMGs/" + Session["nPhoto"].ToString();
                    }
                }
                else
                {
                    lblres.Text = "Formato de imagen no valido";
                    lblres.ForeColor = System.Drawing.Color.Crimson;
                }
            }
            else
            {
                lblres.Text = "Archivo no subido o esta vacio";
                lblres.ForeColor = System.Drawing.Color.Crimson;
            }
        }
        else
        {
            lblres.Text = "No ha cargado ninguna imagen";
            lblres.ForeColor = System.Drawing.Color.Crimson;
        }
    }

    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        int codicate = Convert.ToInt32(dropncat.SelectedValue);
        string nNombre = txtnnombre.Text.Trim();
        decimal precio = Convert.ToDecimal(txtprecio.Text.Trim());
        string ndescrip = txtndescrip.Text.Trim();
        string ntipo = txtntipo.Text.Trim();
        string nest = dropest.SelectedValue;
        int codiprov = Convert.ToInt32(ndropprov.SelectedValue);
        string img = "";
        int idprod = Convert.ToInt32(lblid.Text);
        if (Session["nPhoto"].ToString() == "")
        {
            img = Session["OldPhoto"].ToString();
        }
        else
        { 
       
                img = Session["nPhoto"].ToString();
                string dir = MapPath("~/IMGs/");
                System.IO.File.Delete(dir + Session["OldPhoto"]);
        }
            
        

        try
        {
            MiLinq.sp_modificarProductos(codicate,nNombre,nest,ntipo,ndescrip,img,codiprov,precio,idprod);
            lblres.Text = "Producto Actualizado Correctamente";
            lblres.ForeColor = System.Drawing.Color.DarkGreen;
            txtndescrip.Text = "";
            txtnnombre.Text = "";
            txtntipo.Text = "";
            gridprod.DataSource = MiLinq.sp_ListarProductos();
            gridprod.DataBind();
        }
        catch(Exception ex)
        {
            lblres.Text = "Producto No se Actualizo";
            lblres.ForeColor = System.Drawing.Color.Crimson;
            txtndescrip.Text = "";
            txtnnombre.Text = "";
            txtntipo.Text = "";
        }
    }
}