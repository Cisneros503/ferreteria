﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="Ferreteria/estilos/Login.css" rel="stylesheet"/>
    <title>LogIn</title>
    <style type="text/css">
        body {
            background-image: url(Ferreteria/images/fondo3.jpg);            
            margin: 0;
            padding: 0;
            font-family: sans-serif;
            background-size: cover;
        }

        .box {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 400px;
            padding: 20px;
            background: rgba(0,0,0,.8);
            box-sizing: border-box;
            box-shadow: 0 15px 25px rgba(0,0,0,.5);
            border-radius: 10px;
        }

            .box h2 {
                margin: 0 0 30px;
                padding: 0;
                color: #fff;
                color: darkseagreen;
                text-align: center;
            }

            .box .inputBox {
                position: relative;
            }

                .box .inputBox input {
                    width: 100%;
                    padding: 10px 0;
                    font-size: 16px;
                    color: #fff;
                    margin-bottom: 30px;
                    border: none;
                    border-bottom: 1px solid #fff;
                    outline: none;
                    background: transparent;
                }

                .box .inputBox label {
                    position: absolute;
                    top: 0;
                    left: 0;
                    padding: 10px 0;
                    font-size: 16px;
                    color: #fff;
                    pointer-events: none;
                    transition: .5s;
                }

                .box .inputBox input:focus ~ label,
                .box .inputBox input:valid ~ label {
                    top: -20px;
                    left: 0;
                    color: #03a9f4;
                    font-size: 12px;
                }

            .box input[type="submit"] {
                background: transparent;
                border: none;
                outline: none;
                color: #fff;
                background: #03a9f4;
                padding: 10px 20px;
                cursor: pointer;
                border-radius: 5px;
            }
        .cFL 
        {
	        float:left;
        }
    </style>
</head>
<body>

<div id="header">
   <center><h1>Sistema de Ventas</h1></center>
</div>
    <form id="form1" runat="server">
      <br /><br /><br /><br /> <br /><br /><br />        
        <div class="box">
            <h2>Iniciar Sesi&oacute;n</h2>           
            
                <div class="inputBox"> 
                    <asp:TextBox ID="txtUsuario" runat="server"  name="" required=""></asp:TextBox>                    
                    <label>Usuario</label>
                </div>
                <div class="inputBox">
                    <asp:TextBox ID="txtContraseña" TextMode="Password" runat="server" type="password" name="" required=""></asp:TextBox>                    
                    <label>Contraseña</label>
                </div>
                <asp:Button ID="BtnIngresar" runat="server" Text="Ingresar" OnClick="btnIngresar_Click" />   
                <asp:Label ID="lblMensaje" CssClass="cFL" runat="server" ForeColor="#996600"></asp:Label>    
            </div>         
            </form>            

<div id="footer">
    <center>Proyecto desarrollado por Estandares de programacion | Ciclo:01-2018 | Universidad Tecnologica de El Salvador</center>
</div>
</body>
</html>
