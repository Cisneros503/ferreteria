﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Ferreteria_maestra_Vendedor : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Lkbbuscar_Click(object sender, EventArgs e)
    {
        Response.Redirect("BuscarProductos.aspx");
    }

    protected void lkbsalir_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Login.aspx");
        Session.Abandon();
        Session.Clear();
    }
}
