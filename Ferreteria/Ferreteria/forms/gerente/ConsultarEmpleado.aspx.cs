﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Ferreteria_forms_gerente_ConsultarEmpleado : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnCosultar_Click(object sender, EventArgs e)
    {
        // Definimos una cadena y le asignamos la cadena de conexión definida en el archivo Web.config
        string cadena = System.Configuration.ConfigurationManager.ConnectionStrings["VentasConnectionString"].ConnectionString;
        /* Creamos un objeto de la clase SQLConnection indicando como parámetro la cadena de conexión
        creada anteriormente */
        SqlConnection conexion = new SqlConnection(cadena);
        // Abrimos la conexión
        conexion.Open();
        // Creamos un objeto de la clase SqlCommand con el dato cargado en el control TextBox
        SqlCommand comando = new SqlCommand("select codi_emp,nomb_emp, apel_emp, tel_emp,dir_emp,DUI_emp from Empleado " + " where nomb_emp='" +
        txtempleado.Text + "'", conexion);
        /* Creamos un objeto de la clase SqlDataReader e inicializándolo mediante la llamada del
        método ExecuteReader de la clase SQLCommand */
        SqlDataReader registro = comando.ExecuteReader();
        /* Recorremos el SqlDataReader (como este caso puede retornar cero o un registro) lo hacemos
        mediante un "if" */
        /* Si el método Read retorna true luego podemos acceder a la fila recuperada con el select y
        la mostramos en la etiqueta */
        if (registro.Read())
            lblMensaje.Text = "Código de Empleado: " + registro["codi_emp"] + "<br>" +
            "Nombres del Empleado: " + registro["nomb_emp"] + "<br>" +
            "Apellidos del Empleado: " + registro["apel_emp"] + "<br>" +
            "Telefono del Empleado: " + registro["tel_emp"] + "<br>" +
            "Direccion del Empleado: " + registro["dir_emp"] + "<br>" +
            "Numero de DUI del Empleado: " + registro["DUI_emp"];
        else // Si retorna "False" mostramos un mensaje informativo
            lblMensaje.Text = "No existe un Empleado con el nombre ingresado. ";
        // Cerramos la conexión
        conexion.Close();
    }
}