﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Ferreteria_forms_gerente_AddProducto : System.Web.UI.Page
{
    VentasDataContext MiLinq = new VentasDataContext();   
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            dropcat.DataSource = MiLinq.sp_ListarCategoria();
            dropcat.DataTextField = "nomb_cate";
            dropcat.DataValueField = "codi_cate";
            dropcat.DataBind();
            dropprov.DataSource = MiLinq.sp_listProveedores();
            dropprov.DataTextField = "nomb_prov";
            dropprov.DataValueField = "codi_prov";
            dropprov.DataBind();
        }
    }

    protected void Carga_Click(object sender, EventArgs e)
    {
        if (subir.HasFile)
        {
            if (subir.PostedFile != null && subir.PostedFile.ContentLength > 0)
            {
                Session["Photo"] = subir.PostedFile.FileName;
                string exten = System.IO.Path.GetExtension(Session["Photo"].ToString()).ToLower();

                if (exten == ".jpg" || exten == ".png" || exten == ".jpeg")
                {
                    string dir = Server.MapPath("~/IMGs/");
                    subir.PostedFile.SaveAs(dir + Session["Photo"].ToString());
                    imgc.ImageUrl = "~/IMGs/" + Session["Photo"].ToString();
                }
                else
                {
                    lblres.Text = "Formato invalido";
                }
            }
            else
            {
                lblres.Text = "La imagen esta Vacia o no subio imagen";
            }
        }
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        
        int codicate = Convert.ToInt32(dropcat.SelectedValue);
        string Nombre = txtnom.Text.Trim();
        string descrip = txtdes.Text.Trim();
        string stado = Dropesta.SelectedValue;
        string tipo = txttipo.Text.Trim();
        string img = Session["Photo"].ToString();
        decimal precio = Convert.ToDecimal(txtPrecio.Text.Trim());
        int prov = Convert.ToInt32(dropprov.SelectedValue);

        try
        {
            MiLinq.sp_AgregarProductos(codicate, Nombre, stado, tipo, descrip, img, prov, precio);
            lblres.Text = "Producto Agregado Correctamente";
            lblres.ForeColor = System.Drawing.Color.DarkGreen;
            txtnom.Text = "";
            txtdes.Text = "";
            txttipo.Text = "";
            txtPrecio.Text = "";
            imgc.ImageUrl = "";
        }
        catch (Exception ex)
        {
            lblres.Text = "El Producto no se agrego";
            lblres.ForeColor = System.Drawing.Color.Crimson;
            txtnom.Text = "";
            txtdes.Text = "";
            txttipo.Text = "";
            txtPrecio.Text = "";
        }
    }
}