﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Ferreteria_forms_gerente_IngresarUsuario : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void BtnIngresar_Click(object sender, EventArgs e)
    {
        LoginService.prIngresarUsuario(this.txtUsuario.Text, this.txtContraseña.Text, this.txtEmail.Text, DateTime.Now, Convert.ToInt32(ddlTipo.SelectedValue));
        
        lblMensaje.Text = "Usuario Insertado correctamente.";
    }
}