﻿<%@ Page Title="" Language="C#" MasterPageFile="../../maestras/Gerente.master" AutoEventWireup="true" CodeFile="Proveedor.aspx.cs" Inherits="Ferreteria_forms_gerente_Proveedor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 51px;
        }
        .auto-style2 {
            width: 50px;
        }
        .auto-style4 {
            width: 47px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    <div align="center">
    <table class="auto-style4" >
        <tr>
            <td  class="auto-style1" colspan="6">
                <h1  class="auto-style2">Proveedores</h1>
            </td>
        </tr>
        <tr>
            <td class="auto-style14">
                <asp:Label ID="Label1" runat="server" Text="Nombre del Proveedor"></asp:Label>
            </td>
            <td class="auto-style1" colspan="2">
                <asp:TextBox ID="txtNomb" runat="server"></asp:TextBox>
            </td>
            <td class="auto-style16" colspan="2">
                <asp:Label ID="Label2" runat="server" Text="Marca"></asp:Label>
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtMarca" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style14">
                <asp:Label ID="Label3" runat="server" Text="Dirección"></asp:Label>
            </td>
            <td class="auto-style1" colspan="2">
                <asp:TextBox ID="txtDir" runat="server"></asp:TextBox>
            </td>
            <td class="auto-style16" colspan="2">
                <asp:Label ID="Label4" runat="server" Text="Telefono"></asp:Label>
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtTel" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style14">
                <asp:Label ID="Label5" runat="server" Text="Razon Social"></asp:Label>
            </td>
            <td class="auto-style1" colspan="2">
                <asp:TextBox ID="txtRazon" runat="server"></asp:TextBox>
            </td>
            <td class="auto-style17" colspan="2">
                &nbsp;</td>
            <td class="auto-style1">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                
                
                <asp:Button ID="btnModificar" runat="server" Text="Modificar" />
            </td>
            <td colspan="2">
                
                
                <asp:Button ID="btnInsertar" runat="server" Text="Insertar" />
            </td>
            <td colspan="2">
                
                
                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" />
            </td>
        </tr>
        
        <tr>
            <td colspan="6">
                
                
                &nbsp;</td>
        </tr>
        
        <tr>
            <td colspan="6">
                
                
                <asp:GridView ID="gvProveedores" AutoGenerateColumns="true" runat="server" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical">
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                    
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="Gray" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                </asp:GridView>
                <br />
                <br />                
            </td>
        </tr>
        
    </table>
</div>

</asp:Content>

