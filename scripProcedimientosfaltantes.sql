use Ventas
go
create Procedure [dbo].[sp_agregarStocks]
@total_prod numeric(12,5),
@codi_prod int
as
begin
insert into Stock values(@total_prod, @codi_prod)
end
go
create Procedure [dbo].[sp_BuscarStocks]
@codiprod int
as
begin
select *from Stock where codi_prod=@codiprod
end
go
create Procedure [dbo].[sp_editarStocks]
@total_prod numeric(12,5),
@codi_prod int,
@idstock int
as
begin
update Stock set total_prod_stock=@total_prod, codi_prod=@codi_prod where id_stock=@idstock
end
go
create procedure sp_verProveedor
@codi_prov int
as
begin
select *from Proveedor where codi_prov=@codi_prov
end
go
create procedure sp_DeleteProveedor
@codiprov int
as
begin
delete from Proveedor where codi_prov=@codiprov
end