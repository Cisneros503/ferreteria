﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ferreteria/maestras/Gerente.master" AutoEventWireup="true" CodeFile="ConsultarUsuario.aspx.cs" Inherits="Ferreteria_forms_gerente_ConsultarUsuario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">    
    <style type="text/css">
        body {
            background-image: url(../images/fondo.jpg);            
            margin: 0;
            padding: 0;
            font-family: sans-serif;
            background-size: cover;
        }

        .box {
            position: relative;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 400px;
            padding: 20px;
            background: rgba(0,0,0,.8);
            box-sizing: border-box;
            box-shadow: 0 15px 25px rgba(0,0,0,.5);
            border-radius: 10px;
        }

            .box h2 {
                margin: 0 0 30px;
                padding: 0;
                color: #fff;
                color: darkseagreen;
                text-align: center;
            }

            .box .inputBox {
                position: relative;
            }

                .box .inputBox input {
                    width: 100%;
                    padding: 10px 0;
                    font-size: 16px;
                    color: #fff;
                    margin-bottom: 30px;
                    border: none;
                    border-bottom: 1px solid #fff;
                    outline: none;
                    background: transparent;
                }

                .box .inputBox label {
                    position: absolute;
                    top: 0;
                    left: 0;
                    padding: 10px 0;
                    font-size: 16px;
                    color: #fff;
                    pointer-events: none;
                    transition: .5s;
                }

                .box .inputBox input:focus ~ label,
                .box .inputBox input:valid ~ label {
                    top: -20px;
                    left: 0;
                    color: #03a9f4;
                    font-size: 12px;
                }

            .box input[type="submit"] {
                background: transparent;
                border: none;
                outline: none;
                color: #fff;
                background: #03a9f4;
                padding: 10px 20px;
                cursor: pointer;
                border-radius: 5px;
            }
        .cFL 
        {
	        float:left;
        }
    </style>    
    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    
        <div class="box">
            <h2>Consultar Usuario</h2>
            <div class="inputBox">
                <asp:TextBox ID="txtUsuario" runat="server" name="" required=""></asp:TextBox>
                <label>Ingrese el nombre de usuario a buscar</label>
            </div>
            <div class="inputBox">
                <asp:Label ID="lblMensaje" CssClass="cFL" runat="server" ForeColor="#996600"></asp:Label>
            </div>

            <asp:Button ID="BtnConsultar" runat="server" Text="Buscar" OnClick="btnCosultar_Click" />

        </div>
</asp:Content>

